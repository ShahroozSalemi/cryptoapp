import styles from "./Pagination.module.css";

const Pagination = ({ page, setPage }) => {
  const previosHandler = () => {
    if (page <= 1) return;
    setPage((page) => {
      return page - 1;
    });
  };
  const nextHandler = () => {
    if (page >= 10) return;
    setPage((page) => {
      return page + 1;
    });
  };

  return (
    <div className={styles.container}>
      <button
        onClick={previosHandler}
        className={page === 1 ? styles.disabled : null}
      >
        Previos
      </button>
      <p className={page === 1 ? styles.selected : null} onClick={() => setPage(1)}>
        1
      </p>
      <p className={page === 2 ? styles.selected : null} onClick={() => setPage(2)}>
        2
      </p>
      {page > 2 && page < 9 && (
        <>
          <span>...</span>
          <p className={ styles.selected}>{page}</p>
        </>
      )}
      <span>...</span>
      <p className={page === 9 ? styles.selected : null} onClick={() => setPage(9)}>
        9
      </p>
      <p className={page === 10 ? styles.selected : null} onClick={() => setPage(10)}>
        10
      </p>
      <button
        onClick={nextHandler}
        className={page === 10 ? styles.disabled : null}
      >
        next
      </button>
    </div>
  );
};

export default Pagination;

Pagination.propTypes = Object;
