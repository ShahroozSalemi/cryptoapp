import { useState } from "react";
import { ConvertData } from "../../helpers/ConvertData";
import styles from "./Chart.module.css";
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

// eslint-disable-next-line react/prop-types
const Chart = ({ chart, setChart }) => {
  // eslint-disable-next-line no-unused-vars
  const [type, setType] = useState("prices");
  const data = ConvertData(chart, type);

  const clickHandler = () => {
    setChart(!chart);
  };
  const typeHandler = (e) => {
    if (e.target.tagName === "BUTTON") {
      const type = e.target.innerText.toLowerCase().replace(" ", "_");
      setType(type)
    }
  };
  return (
    <div className={styles.container}>
      <span className={styles.cross} onClick={clickHandler}>
        X
      </span>
      <div className={styles.chart}>
        <div className={styles.name}>
          <img src={chart.coin.image} />
          <p>{chart.coin.name}</p>
        </div>
        <div className={styles.graph}>
          <ChartComponent type={type} data={data} />
        </div>
        <div className={styles.types} onClick={typeHandler}>
          <button className={type === "prices" ? styles.selected : null}>Prices</button>
          <button className={type === "market_caps" ? styles.selected : null}>Market Caps</button>
          <button className={type === "total_volumes" ? styles.selected : null}>Total Volumes</button>
        </div>
        <div className={styles.details}>
          <div>
            <p>Prices :</p>
            <span> $ {chart.coin.current_price}</span>
          </div>
          <div>
            <p>ATH :</p>
            <span> $ {chart.coin.ath}</span>
          </div>
          <div>
            <p>Market Cap :</p>
            <span> $ {chart.coin.market_cap.toLocaleString()}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Chart;

// eslint-disable-next-line react/prop-types
const ChartComponent = ({ type, data }) => {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart width={400} height={400} data={data}>
        <Tooltip />
        <CartesianGrid stroke="#404042" />
        <Line
          type="monotone"
          dataKey={type}
          stroke="#3874ff"
          strokeWidth="2px"
        />
        <YAxis dataKey={type} domain={["auto", "auto"]} />
        <XAxis dataKey="date" hide />
        <Legend />
      </LineChart>
    </ResponsiveContainer>
  );
};
