import ChartUp from "../../assets/chart-up.svg";
import ChartDown from "../../assets/chart-down.svg";
import { RotatingLines } from "react-loader-spinner";
import styles from "./TableCoin.module.css";
import { useState } from "react";
import { chartData } from "../../services/CryptoApi";

let moneySymbol = "";
const TableCoin = ({ coins, isLoading, currency, setChart }) => {
  if (currency === "eur") {
    moneySymbol = "€";
  } else if (currency === "jpy") {
    moneySymbol = "¥";
  } else moneySymbol = "$";

  return (
    <div className={styles.container}>
      {isLoading ? (
        <RotatingLines
          strokeColor="blue"
          strokeWidth="5"
          animationDuration="0.75"
          width="96"
          visible={true}
        />
      ) : (
        <table className={styles.table}>
          <thead>
            <tr>
              <th>Coin</th>
              <th>Name</th>
              <th>Price</th>
              <th>24H</th>
              <th>total volume</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {coins.map((item) => (
              <TableRow coin={item} key={item.id} setChart={setChart} />
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

TableCoin.propTypes = Array;

export default TableCoin;

const TableRow = ({ setChart, coin }) => {
  const {
    id,
    name,
    image,
    symbol,
    current_price,
    price_change_percentage_24h,
    total_volume,
  } = coin;
  const clickHandler = async () => {
    try {
      const res = await fetch(chartData(id));
      const json = await res.json();
      setChart({ ...json, coin: coin });
    } catch (error) {
      setChart(null);
    }
  };
  return (
    <tr>
      <td>
        <div className={styles.symbol} onClick={clickHandler}>
          <img src={image} alt="" />
          <span>{symbol.toUpperCase()}</span>
        </div>
      </td>
      <td>{name}</td>
      <td>
        {moneySymbol} {current_price.toLocaleString()}
      </td>
      <td
        className={
          price_change_percentage_24h > 0 ? styles.success : styles.error
        }
      >
        {price_change_percentage_24h.toFixed(2)}%
      </td>
      <td>
        {moneySymbol} {total_volume.toLocaleString()}
      </td>
      <td>
        <img
          src={price_change_percentage_24h > 0 ? ChartUp : ChartDown}
          alt=""
        />
      </td>
    </tr>
  );
};
