import styles from "./Layout.module.css";

const Layout = ({ children }) => {
  return (
    <>
      <header>
        <h1>Crypto App</h1>
      </header>
      {children}
      <footer>
        <p>Develop by Shahrooz</p>
      </footer>
    </>
  );
};

export default Layout;
