import Layout from "./Layout/Layout";
import HomePage from "./components/template/HomePage";

function App() {
  return (
    <>
      <Layout>
        <HomePage />
      </Layout>
    </>
  );
}

export default App;
